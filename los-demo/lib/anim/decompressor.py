import os, bz2, gzip

__VERSION__ = "3.1"

DEBUG = False

def decompress(filename):
    
    if DEBUG: print "Trying to decompress", filename, "...",
    
    tmp = open(filename+".tmp", "wb")
    
    is_bz2 = False
    try:
        f = open(filename, "rb")
        tmp.write(bz2.decompress(f.read()))
        is_bz2 = True
        f.close()
    except IOError, e:
        if DEBUG: print e

    is_gzip = False
    if is_bz2 == False:
        try: 
            f = gzip.open(filename, "rb")
            tmp.write(f.read())
            is_gzip = True
            f.close() 
        except IOError, e:
            if DEBUG: print e
            
    tmp.close()
    
    
    if is_gzip == True:
        os.unlink (filename)
        os.rename(filename+".tmp", filename)
        if DEBUG: print "..decompressed from gzip"
        return
    
    if is_bz2 == True:
        os.unlink (filename)
        os.rename(filename+".tmp", filename)
        if DEBUG: print "..decompressed from bz2"
        return
            
    os.unlink(filename+".tmp")
    return
    