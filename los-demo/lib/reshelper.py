import sys, os
import anim
import tupleVector

class ResourceHelper (object):
    """
    Class to load Game objects and tag them correctly on the grid
    """
    def __init__(self, world):
        self._resourceDirt = []
        self._resourceShadow = []
        self.world = world
        self.grid = self.world.grid
        
    def registerUsedTilesFromSurface (self, img, screenPos, tag):
        """
        every Tile occupied by "img" on "screenpos" will be tagged with "tag" 
        """
        wPi = self.grid.WTilePi
        hPi = self.grid.HTilePi
        rImg = img.get_rect()
        x = y = 0
        for h in xrange (0, rImg.h / hPi):
            for w in xrange (0, rImg.w / wPi):
                x = (w * wPi) + 1
                y = (h * hPi) + 1
                pos = tupleVector.vAdd (screenPos, (x,y))
                tile = self.grid.get_tile_at_pixelpos (pos)
                self.grid.register (tile, tag)
    
    def registerBlockingTilesFromSurface (self, img, screenPos, blockingColor = (255,0,0), blockingTile = "blocking"):
        """
        Analyse blocking.png to find blocking tiles
        if tilesize is (10,10) we read every 5 (=10/2) pixel to determine tiles
        if pixel == (255,0,0) register tile as "blocking" on grid
        "blocking" is used by astar
        """
        wPi = self.grid.WTilePi
        hPi = self.grid.HTilePi
        offsetW = wPi / 2
        offsetH = hPi / 2
        rImg = img.get_rect()
        
        x = y = 0
        for h in xrange (0, rImg.h / hPi):
            for w in xrange (0, rImg.w / wPi):
                x = (w * wPi) + offsetW
                y = (h * hPi) + offsetH
                if img.get_at((x,y))[:3] == blockingColor:
                    pos = tupleVector.vAdd (screenPos, (x,y))
                    tile = self.grid.get_tile_at_pixelpos (pos)
                    self.grid.register (tile, blockingTile)
                            
    def prefetchImageSize (self, filename):
        """
        return pygame.rect with size of resource image
        """
        gfx = anim.AnimNew (filename)
        return gfx.frames[0].image.get_rect()
        
    def loadBuildingTar (self, filename, pos):
        """ load a building tar and blit dirt and shadow layer on bgimage"""
        shadow = dirt = frame0 = None
        depth = rel_depth = (0,0)
        gfx = anim.Anim ()
        gfx.register_command ("Hotspot", "self.hotspot=", "")
        gfx.load (filename)
        for f in gfx.frames:
            if f.name == "shadow.png":
                self._resourceShadow.append ((f.image.convert_alpha(), pos))
            if f.name == "dirt.png":
                self._resourceDirt.append ((f.image.convert_alpha(),pos))
            if f.name == "blocking.png":
                btiles = self.registerBlockingTilesFromSurface(f.image, pos)
            if f.name == "blocking_insideanim.png":
                btiles = self.registerBlockingTilesFromSurface(f.image, pos, (255,0,0), "blockinside")
            if f.name == "frame0.png":
                frame0 = f.image
                rel_depth= gfx.animdat.hotspot.hotspot
        #depth = tupleVector.vAdd (pos, rel_depth)
        #try:
        #    zDepth = self.grid.getTileAtPi (tDepthPi).getZDepth()
        #except:
        #    zDepth = 0
        return frame0, rel_depth
    
    def renderResourceDirt (self):
        for d in self._resourceDirt:
            self.world.background.blit (d[0], d[1])
    
    def renderResourceShadow (self):
        for s in self._resourceShadow:
            self.world.background.blit (s[0], s[1])
