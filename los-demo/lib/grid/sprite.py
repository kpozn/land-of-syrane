
import path
import math
import tuplevector as tv
import unit

# this class has no (more) a position handling !!! 
# and does not register in zBuffer anymore !!!

class Sprite(object):
    def __init__(self):
        self._heading = 0           # in radians (internally)
        self._path = path.Path()
    def __repr__(self):
        return "<Blocksprite "+str(self)+">"
    def _set_heading (self, degree): 
        self._heading = math.radians(degree)
        return True
    def _get_heading (self): 
        return math.degrees(self._heading) 
    def _set_path (self, path): 
        self._path.setPath(path)
        return True
    def _get_path (self): 
        return self._path
    path = property (_get_path, _set_path, doc="path for the sprite to trace")
    heading = property (_get_heading, _set_heading, "heading in angle degree (not radians)")
    
class Gridsprite (Sprite):
    """ 
    Sprite which is grid-aware and has all the features
    """
    def __init__(self, grid):
        Sprite.__init__(self)
        #unit.Position.__init__(self, (grid.WTi, grid.HTi))
        self.rect = unit.Tilerect ((grid.WTilePi, grid.HTilePi))
        self._grid = grid
        self._depth = -1
    def _set_zdepth (self, depth): 
        if self._grid.get_zbuffer() != None:
            zb = self._grid.get_zbuffer()
            if self.depth != -1:
                zb.unregister (self, self._depth)
            self._depth = depth
            zb.register (self, self._depth)
            return True
        return False
    def _get_zdepth (self): 
        return self._depth
    depth = property (_get_zdepth, _set_zdepth, doc="zBuffer depth")
    def simulate_move_forward(self, speed):
        if speed == 0: return None
        # ROUND since, we can only handle integers as coordinates!
        dy = round(speed * math.cos(self._heading))
        dx = round(speed * math.sin(self._heading))
        res = (dx,dy)
        return res  
    def move_forward(self, speed):
        self.rect.topleft = tv.vAdd (self.simulate_move_forward (speed), self.rect.topleft)
        return True