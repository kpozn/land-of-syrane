# create proper module namespace
__VERSION__ = "2.1"

from blockengine import Tile, Grid
from astar import Pathfinding_Grid
from sprite import Gridsprite
from path import Path

def get_version (): return __VERSION__