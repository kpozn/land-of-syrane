
class Path(object):
    """ 
    Path is list of tiles, every call to next() will pop one element out and 
    put it into current and keep a history for backtracking
    """
    def __init__(self, path=None):
        if path != None and isinstance(path, list):
            self._path = path
        else:
            self._path = []
        self._history = []
        self.current = None
        self.destination = None
    def __repr__(self):
        return "<Path:" + str(self._path) + ">"
    def __set_path(self, path):
        if path != None and isinstance(path, list):
            self._path = path
            self._history = []
            self.current = None
            self.destination = self._path[0]
    def __get_path(self): return self._path
    def get_history(self): return self._history
    def next(self): 
        if self._path == []:
            return None
        self.current = self._path.pop()
        self._history.append(self.current)
        return self.current
    path = property (__get_path, __set_path, doc="Path as list")
   