# version 1.1 - 20080630 - rename functions from e.g. Add to vAdd
#                        - rename tuple to tup for conflict with type reasons
#                        - typecast list to tuples
# version 1.2 - 20080918 - added hotshot testing 
#                        - changed everything back to performance optimized versions
#                        - supports only tuples of len 2 !!!

import math

def vAdd (tupA, tupB):
    ''' Add Tuple A and B'''
    return (tupA[0]+tupB[0], tupA[1]+tupB[1])

def vSub (tupA, tupB):
    ''' Sub Tuple A and B'''
    return (tupA[0]-tupB[0], tupA[1]-tupB[1])

def vScalar (tup, scalar):
    return (tup[0] * scalar, tup[1] * scalar)

def vScalarDiv (tup, scalar):
    return (tup[0] / scalar, tup[1] / scalar)
    
def vMul (tupA, tupB):
    return (tupA[0] * tupB[0], tupB[1] * tupB[1])

def vDotMul (tupA, tupB):
    sum = 0
    c = 0
    for i in tupA:
        sum += i * tupB[c]
        c+=1
    return sum

def vLength (tup):
    sum = 0
    for i in tup:
        sum += i ** 2 
    return math.sqrt (sum)       

def vNormalize (tup, normalTolerance=1):
    l = vLength (tup)
    if l < normalTolerance:
        l = normalTolerance
    n = []
    for i in tup:
        n.append(i / l)
    return tuple(n)

def vAngle (tupA, tupB):
    # angle between tupA, tupB
    if vLength(tupA) * vLength(tupB) != 0:
        return math.degrees(math.acos(vDotMul(tupA, tupB) / (vLength(tupA) * vLength(tupB))))
    return 0


def main():
    for i in xrange (0,1000000):
        vAdd((1,1),(2,2))

if __name__ == "__main__":
    import sys
    
    if "profile" in sys.argv:
        import hotshot
        import hotshot.stats
        import tempfile
        import os
 
        profile_data_fname = tempfile.mktemp("prf")
        try:
            prof = hotshot.Profile(profile_data_fname)
            prof.run('main()')
            del prof
            s = hotshot.stats.load(profile_data_fname)
            s.strip_dirs()
            print "cumulative\n\n"
            s.sort_stats('cumulative').print_stats()
            print "By time.\n\n"
            s.sort_stats('time').print_stats()
            del s
        finally:
            # clean up the temporary file name.
            try:
                os.remove(profile_data_fname)
            except:
                # may have trouble deleting ;)
                pass
    else:
        try:
            main()
        except:
            traceback.print_exc(sys.stderr)