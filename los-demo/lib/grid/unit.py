
import pygame


#
# REMINDER: can only save INTEGERS !!
# 
class Tilerect (pygame.Rect):
    def __init__(self, tile_size):
        pygame.Rect.__init__(self, (0,0), (0,0))
        self._tile_w, self._tile_h = tile_size
        
    def __str__(self):
        return self.__repr__()
    def __repr__(self):
        return "<tilerect:"+str((self.x,self.y,self.w,self.h))+"/"+str((self.tile_x,self.tile_y,self.tile_w,self.tile_h))+">"
 
    tile_x = tile_left = property (lambda self: self.x / self._tile_w, doc = "(RO) x,left in tile units")
    tile_y = tile_top = property (lambda self: self.y / self._tile_h, doc = "(RO) y,top in tile units")
    tile_w = tile_width = property (lambda self: self.w / self._tile_w, doc = "(RO) w in tile units")
    tile_h = tile_height = property (lambda self: self.h / self._tile_h, doc = "(RO) h in tile units")

    tile_bottom = property (lambda self: self.tile_top + self.tile_h , doc = "(RO) bottom") 
    tile_right = property (lambda self: self.tile_left + self.tile_w, doc = "(RO) right")   
    
    # tuples:
    tile_size = property (lambda self: (self.tile_w, self.tile_h), doc="(Ro) size")
    tile_topleft = property (lambda self: (self.tile_x, self.tile_y), doc="(Ro) topleft")
    
    # TODO:
    #bottomleft, topright, bottomright
    #midtop, midleft, midbottom, midright
    #center, centerx, centery

    

# Some basic unit tests
if __name__=="__main__":
    print "Set tilesize to (10,10)"
    p = Tilerect ((10,10))
    p.topleft = (100,200)
    p.size = (300,450)
    print p.tile_x, p.tile_y, p.tile_w, p.tile_h
    p[0] = float(935.1)
    p[1] = 425.05
    p[2] = 224
    p[3] = 462
    print "Pixel:",p.x, p.y, p.w, p.h
    print p.tile_x, p.tile_y, p.tile_w, p.tile_h
    print "Pixel T,L,B,R:", p.top, p.left, p.bottom, p. right
    print "Tile T,L,B,R :", p.tile_top, p.tile_left, p.tile_bottom, p.tile_right
    
    print "Tile Size:", p.size, p.tile_size
    print "Topleft, pixel vs tiles", p.topleft, p.tile_topleft