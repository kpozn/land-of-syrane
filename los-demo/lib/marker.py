# -*- coding: utf-8 -*-

# version 1.0 2008-11-27 cookie.michael
# version 1.1 2008-11-30 Michael (cookie.michael1@googlemail.com)
# version 1.2 2009-03-01 Michael - cleanup for new codebases

__VERSION__ = "1.2"
__AUTHOR__ = "michael"

import pygame

class Marker (object):
    """
    Marker handles the selections rectangle on the screen
    by default draws a yellow rectangle onto the selected screen region
    """
    def __init__(self, color=(255,255,0), alpha=100, useRect=True):
        self.pos = (0,0)
        self.rect = None
        self.marking = False
        self.color = color
        self.alpha = alpha
        self.useRect = useRect

    def start (self, clickpos):
        "Start this marker by passing a start point"
        self.marking = True
        self.pos = clickpos
        self.rect = pygame.Rect(clickpos, (0,0))
        
    def end (self):
        "End this marker, return the rect of the marked area"
        self.marking = False    
        return self.rect
        
    def isMarking (self): return self.marking

    def motion (self, motionpos):
        "Notify marker to change the selections rect size when mouse motion happend"
        d = motionpos[0] - self.pos[0] , motionpos[1] - self.pos[1]    
        self.rect = pygame.Rect (self.pos,d)

    def __negateRect__ (self):
        "the rect may have negative size, convert to positive"
        if self.rect.w < 0:
            self.rect.left += self.rect.w 
        if self.rect.h < 0:
            self.rect.top += self.rect.h 
        self.rect.size = abs(self.rect.w), abs(self.rect.h)

    def draw (self, destSurface):
        "Draw marker to surface"
        if not self.marking: return
        self.__negateRect__()
        s= pygame.Surface (self.rect.size)
        s.fill (self.color)
        s.set_alpha (self.alpha, pygame.RLEACCEL)
        if self.useRect: pygame.draw.rect (destSurface, self.color, self.rect, 2)
        destSurface.blit (s, self.rect.topleft)

# Global object to be used
mark = Marker()

def handleMarkingEvents (event):
    global mark
    
    if event.type == pygame.MOUSEBUTTONDOWN:
        if event.button == 1:
            mark.start (event.pos)    
        
    if mark.isMarking():
        if event.type == pygame.MOUSEMOTION:
            mark.motion (event.pos)

# module style wrappers for some class methods 
def drawMarkingArea (destSurface):
    mark.draw(destSurface)

def getMarkRect ():
    return mark.end()
if __name__ == "__main__":
    
    import pygame,sys
    
    pygame.init()
    
    SCR_W, SCR_H = SCRRES = (800,600)
    SCRDEP = pygame.display.mode_ok(SCRRES, pygame.HWSURFACE)
    SCREEN = pygame.display.set_mode(SCRRES, pygame.HWSURFACE, SCRDEP)
        
    running = True            
    m = Marker()       

    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: 
                running = False
            if event.type == pygame.KEYUP:            
                if event.key == pygame.K_ESCAPE:
                    running = False
    
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    m.start (event.pos)
            
            if event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1:
                    m.end()
            
            if m.isMarking():
                if event.type == pygame.MOUSEMOTION:
                    m.motion (event.pos)
    
        SCREEN.fill ((0,0,0))
        SCREEN.blit (bild, (0,0))
       
        m.draw (SCREEN)
        
        pygame.time.wait(2)
        pygame.display.flip()
    
    pygame.quit()
    sys.exit()
