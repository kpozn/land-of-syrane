# MakeHuman cleanup script.
#
# by ThomasL (Thomas Larsson) 17-Oct-08 from belnderartists.org
# modfied to my needs (michael)
#
# Tested with MakeHuman 0.9.1 and Blender 2.48. Hopefully MakeHuman 1.0 
# will make this script obsolete.
# 
# This script follows the MakeHuman to Blender Tutorial by Penix1 at 
# Blender underground, 
# http://blenderunderground.com/2008/0...torial-series/
# Credits to Penix1 for his clear exposition.
#
# Instructions for using this script:
#
# 1. Create a model in MakeHuman. Export as Collada.
#
# 2. Copy (don't move) all files from the location where MakeHuman
# keeps its texture images (on my Windows machine it is
#   C:\Program\MakeHuman 0.9.1 RC1\data\rib_data\textures_data) to a 
# directory of choice. Make the variable TexDir point to this directory:
 
TexDir = 'C:\\Thomas\\rendering\\'
#
# 3. Start a new Blender and delete the default cube.
#
# 4. Run Collada 1.4.0 plugin for Blender with
# 'Only Import Main Scene' and 'New Scene' selected
#
# 5. Select everything (mesh-Geometry-scene and joint0) and rotate it 
# 90 degrees around the X axis, R X 90. Also make it smooth.
# 
# 6. Select mesh-Geometry-scene, tab into edit mode, select a vertex,
# and hit P (Mesh->Vertices->Separate). Choose Separate By Material.
# 
# 7. Execute this script. Hopefully you will have the textured meshes as
# separate objects on layer 1, the armature with descent bone names on 
# layer 2, and the skeleton on layer 3.
#
# 8. For obscure reasons, the armature does not show up on layer 2, although it 
# is there. To see it, tab in and out of edit mode at once, or select it in the outliner.
#
# Things that could be better, but that I don't know how to do:
# 
# 1. Automate smoothing and initial rotation. 
# 2. Automate splitting of mesh-Geometry-scene.
# 3. Separate eye, iris, eyebrows and eyelashes into separate left-right
# objects.
# 4. Recenter the separated objects. As of now, all mesh objects have the same
# center as the original mesh-Geometry-scene object.
#
 
import Blender
from Blender import Scene, Object, Window, Material, Texture, Image, Mathutils
from Blender.Texture import *
from Blender.Material import *
 
#
# cleanScenes ()
#
def cleanScenes ():
 scn = Scene.Get('Scene')
 scn001 = Scene.Get('Scene.001')
 Scene.Unlink(scn001)
 scn001 = Scene.Get('Scene001')
 obList = scn001.getChildren()
 for ob in obList:
  scn.link(ob)
 scn.makeCurrent()
 Scene.Unlink(scn001)
 #end cleanScenes
#
# renameObject(oldName, newName)
#
def renameObject(oldName, newName):
 ob = Object.Get(oldName)
 print oldName, newName, ob.colbits
 ob.name = newName
 me = ob.getData(0, 1)
 me.name = newName
 me.smooth()	 # Does not seem to work?
 mat = Material.Get(newName)
 for i in range(9):
  print mat, me.materials[i]
  if me.materials[i] != mat:
   me.materials[i] = None
 ob.layers = [1]
 return ob
 
#
# def renameAll ()
#
def renameAll ():
 renameObject('mesh-Geometry-sce', 'iris')
 renameObject('mesh-Geometry-sce.001', 'head')
 renameObject('mesh-Geometry-sce.002', 'eye')
 renameObject('mesh-Geometry-sce.003', 'body')
 renameObject('mesh-Geometry-sce.004', 'eyelashes')
 renameObject('mesh-Geometry-sce.005', 'teeth')
 ob = renameObject('mesh-Geometry-sce.006', 'bones')
 renameObject('mesh-Geometry-sce.007', 'tongue')
 renameObject('mesh-Geometry-scene', 'eyebrows')
 ob.layers = [3]
 ob = Object.Get('joint0')
 ob.layers = [2]
 #end renameAll ()
 
#
# liftObjectUVs(obName, dist)
#
def liftObjectUVs(obName, dist):
 print "Lifting ", obName
 ob = Object.Get(obName)
 Window.EditMode(1)
 me = ob.getData(0, 1)
 vec = Mathutils.Vector(0.0, 1.0)
 if me.faceUV:
  for f in me.faces:
   uv = f.uv
   f.uvSel = tuple([1, 1, 1])
   uv0 = uv[0] + vec
   uv1 = uv[1] + vec
   uv2 = uv[2] + vec
   f.uv = tuple([uv0, uv1, uv2])
   f.uvSel = tuple([0, 0, 0])
#   print "f ", f.uv, "sel ", f.uvSel
 Window.EditMode(0)
 #end liftObjectUVs
#
# liftAllUVs()
#
def liftAllUVs():
 liftObjectUVs('head', 512)
 liftObjectUVs('tongue', 512)
 liftObjectUVs('teeth', 512)
 liftObjectUVs('body', 1024)
 liftObjectUVs('eyelashes', 256)
 liftObjectUVs('eyebrows', 256)
 liftObjectUVs('eye', 256)
 #end liftAllUVs
#
# loadTexture(mat, index, name, mapto)
#
def loadTexture(mat, index, name, mapto):
 print 'New texture ', name
 tex = Texture.New(name)
 tex.setType('Image')
 filename = TexDir + name + '.tif'
 print 'Loading ', filename
 img = Image.Load(filename)
 tex.image = img
 mat.setTexture(index, tex, TexCo.UV, mapto)  
 
#
# setMaterial(obName, n, mat)
#
def setMaterial(obName, n, mat):
 ob = Object.Get(obName)
 me = ob.getData(0, 1)
 me.materials[n] = mat 
#
# makeMaterials ()
#
# Set material properties and load texture files.
#
def makeMaterials ():
# Body material 
 bodymat = Material.Get('body')
 bodymat.setSpec(0)
 bodymat.setHardness(1)
 loadTexture(bodymat, 0, 'body_color', (MapTo.COL | MapTo.CSP | MapTo.CMIR))  
 loadTexture(bodymat, 1, 'body_bump', MapTo.NOR)
 loadTexture(bodymat, 2, 'body_specular', MapTo.HARD)
 tex = bodymat.getTextures()[1]
 tex.noRGB = True
 tex2 = bodymat.getTextures()[2]
 tex2.noRGB = True
# Head material 
 headmat = Material.Get('head')
 headmat.setSpec(0)
 headmat.setHardness(1)
 loadTexture(headmat, 0, 'head_color', (MapTo.COL | MapTo.CSP | MapTo.CMIR))  
 loadTexture(headmat, 1, 'head_bump', MapTo.NOR)
 loadTexture(headmat, 2, 'head_specular', MapTo.HARD)
 tex = headmat.getTextures()[1]
 tex.noRGB = True
 tex2 = headmat.getTextures()[2]
 tex2.noRGB = True
 setMaterial('teeth', 6, headmat)
 setMaterial('tongue', 8, headmat)
# eyelashes material 
 lashmat = Material.Get('eyelashes')
 lashmat.setSpec(0)
 lashmat.setHardness(1)
 lashmat.setAlpha(0.0)
 lashmat.mode |= Modes.ZTRANSP
 lashmat.mode &= ~ (Modes.SHADOWBUF | Modes.TRACEABLE)
 loadTexture(lashmat, 0, 'eyelashes_color', (MapTo.COL | MapTo.ALPHA))  
# eyebrows material 
 browmat = Material.Get('eyebrows')
 browmat.setSpec(0)
 browmat.setHardness(1)
 browmat.setAlpha(0.0)
 browmat.mode |= Modes.ZTRANSP
 browmat.mode &= ~ (Modes.SHADOWBUF | Modes.TRACEABLE)
 loadTexture(browmat, 0, 'eyebrows_color', MapTo.COL)  
 loadTexture(browmat, 1, 'eyebrows_alpha', MapTo.ALPHA)  
 tex = browmat.getTextures()[1]
 tex.noRGB = True
# Eyes material 
 eyesmat = Material.Get('eye')
 loadTexture(eyesmat, 0, 'eyes_color', MapTo.COL)  
 loadTexture(eyesmat, 1, 'eyes_reflection', MapTo.SPEC)
 tex = eyesmat.getTextures()[1]
 tex.noRGB = True
 #end makeMaterials
 
#
# renameBone ()
#
def renameBone (oldName, newName):
 global armData 
 b = armData.bones[oldName]
 b.name = newName
#
# renameJoints ()
#
# Give the bones some descriptive names
#
def renameJoints ():
 print "Renaming joints"
 renameBone("joint0", "Root")
 renameBone("joint1", "Spine")
 renameBone("joint2", "Neck")
 renameBone("joint3", "Head")
# Left Arm
 renameBone("joint4", "Clavicle_L")
 renameBone("joint6", "UpArm_L")
 renameBone("joint10", "LoArm_L")
 renameBone("joint14", "Hand_L")
 renameBone("joint15", "Fin4_1_L")
 renameBone("joint16", "Fin4_2_L")
 renameBone("joint17", "Fin4_3_L")
 renameBone("joint18", "Fin3_1_L")
 renameBone("joint19", "Fin3_2_L")
 renameBone("joint20", "Fin3_3_L")
 renameBone("joint21", "Fin2_1_L")
 renameBone("joint22", "Fin2_2_L")
 renameBone("joint23", "Fin2_3_L")
 renameBone("joint24", "Fin1_1_L")
 renameBone("joint25", "Fin1_2_L")
 renameBone("joint26", "Fin1_3_L")
 renameBone("joint27", "Fin0_1_L")
 renameBone("joint28", "Fin0_2_L")
 renameBone("joint29", "Fin0_3_L")
# Right arm
 renameBone("joint5", "Clavicle_R")
 renameBone("joint7", "UpArm_R")
 renameBone("joint11", "LoArm_R")
 renameBone("joint30", "Hand_R")
 renameBone("joint31", "Fin4_1_R")
 renameBone("joint32", "Fin4_2_R")
 renameBone("joint33", "Fin4_3_R")
 renameBone("joint34", "Fin3_1_R")
 renameBone("joint35", "Fin3_2_R")
 renameBone("joint36", "Fin3_3_R")
 renameBone("joint37", "Fin2_1_R")
 renameBone("joint38", "Fin2_2_R")
 renameBone("joint39", "Fin2_3_R")
 renameBone("joint40", "Fin1_1_R")
 renameBone("joint41", "Fin1_2_R")
 renameBone("joint42", "Fin1_3_R")
 renameBone("joint43", "Fin0_1_R")
 renameBone("joint44", "Fin0_2_R")
 renameBone("joint45", "Fin0_3_R")
# Left leg
 renameBone("joint8", "UpLeg_L")
 renameBone("joint12", "LoLeg_L")
 renameBone("joint46", "Foot_L")
 
 renameBone("joint47", "Toe0_1_L")
 renameBone("joint48", "Toe0_2_L")
 
 renameBone("joint49", "Toe1_1_L")
 renameBone("joint50", "Toe1_2_L")
 
 renameBone("joint51", "Toe2_1_L")
 renameBone("joint52", "Toe2_2_L")
 
 renameBone("joint53", "Toe3_1_L")
 renameBone("joint54", "Toe3_2_L")
 
 renameBone("joint55", "Toe4_1_L")
 renameBone("joint56", "Toe4_2_L")
# Right leg
 renameBone("joint9", "UpLeg_R")
 renameBone("joint13", "LoLeg_R")
 renameBone("joint57", "Foot_R")
 
 renameBone("joint58", "Toe0_1_R")
 renameBone("joint59", "Toe0_2_R")
 
 renameBone("joint60", "Toe1_1_R")
 renameBone("joint61", "Toe1_2_R")
 
 renameBone("joint62", "Toe2_1_R")
 renameBone("joint63", "Toe2_2_R")
 
 renameBone("joint64", "Toe3_1_R")
 renameBone("joint65", "Toe3_2_R")
 
 renameBone("joint66", "Toe4_1_R")
 renameBone("joint67", "Toe4_2_R")
 #end rename_joints()
#
# renameArmature()
#
def renameArmature ():
 global armData
 #armOb = Object.Get('joint0')
 armOb = Object.Get('Armature')
 armOb.name = "Armature"
 armData = armOb.getData()
 armData.makeEditable()
 renameJoints()
 armData.update()
 Window.EditMode(0)
 armOb.sel = 1
 #end renameArmature
#
# And put everything together...
#
def fixMhImport():
 ob = Object.Get('mesh-Geometry-sce')
 cleanScenes ()
 renameAll()
 makeMaterials ()
 liftAllUVs()
 renameArmature()
 #end fixMhImport
 
try:
 fixMhImport()
except:
 str = 'Error. Have you separated the mesh by materials?'
 Blender.Draw.PupMenu(str)