# Lazy resource manager
# TODO: replace or remove if not needed

import pygame

class ResourceManager (object):
    """
    Keeps track of pygame-related resources from a dict    
    """
    
    def __init__(self):
        self._res = {}
        
    def load_surface (self, filename):
        try:
            s = self._res [filename]
        except KeyError:
            s = pygame.image.load (filename)
            self._res [filename] = s
        return s