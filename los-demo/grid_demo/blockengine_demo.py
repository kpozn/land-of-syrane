# -*- coding: utf-8 -*-
# License: GPL V3
# 2009-02-13 demo to test new blockengine



import pygame, os, sys
sys.path.append(os.path.join("..","lib"))
import grid
import random


class Sprite (grid.Gridsprite):
    def __init__(self, img, g):
        grid.Gridsprite.__init__ (self, g)
        self.osurf = self.surface = img
        self.img_rect = None
        if img != None:
            self.img_rect = self.surface.get_rect()
        self.colCircleRad = 0
    
    # we do rotate the image here by surface transformation
    def rotate (self):
        self.surface = pygame.transform.rotate(self.osurf, self.heading)
        self.rRect= self.surface.get_rect()
    def draw (self, screen, grid):
        screen.blit (self.surface, grid.g2s(self.rect.center))
    
    def set_col_circle (self, radius): self.colCircleRad = radius
    def get_col_circle (self): return self.colCircleRad
    def collide_circle (self, other):
        l = vLength (vSub(self.rect.center, other.rect.center))
        if l <= self.colCircleRad + other.colCircleRad:
            return True
        return False    
    
 

pygame.init()

SCRRES = SCR_W, SCR_H = (800,600)
SCRDEP = pygame.display.mode_ok(SCRRES)
SCREEN = pygame.display.set_mode(SCRRES, 0, SCRDEP)

font =  {
     8 : pygame.font.Font (os.path.join("res","DejaVuSans.ttf"), 8),
     18: pygame.font.Font (os.path.join("res","DejaVuSans.ttf"), 18)
    }

g = grid.Pathfinding_Grid((0,0), 50, 50, (14,10))

treelist = []
#t = pygame.image.load ("tree.png").convert_alpha()
t = pygame.image.load (os.path.join("res", "woodchopper.png"))
t.convert()
t.set_colorkey ((255,0,255), pygame.RLEACCEL)
rt = t.get_rect()
for i in range(0, 1): #random.random()*50):
    x=rt.w + random.random()*(700-rt.w)
    y=rt.h + random.random()*(500-rt.h)
    tile = g.get_tile_at_pixelpos((x,y))
    tree = Sprite(t, g)
    tree.rect.center = (x,y)
    treelist.append(tree)
    g.register(tree, "tree")
    g.register(tree, "blocking")
    g.register(tree, tile)
    
running = True


man = []
for i in xrange(0,1):
    man.append(Sprite(pygame.image.load (os.path.join("res","woodzy.png")).convert_alpha(), g))
    g.register(man[-1], "man")
    g.register(man[-1], "blocking")
    g.register(man[-1], tile)

#tile = None
tile = g.get_tile_at_pixelpos((50,50))
man[0].heading = g.get_heading_to_target(man[0].rect.center, tile)
man[0].rotate()


path = None
timer = pygame.time.Clock()

#g.printTagRegister(2)

fps_max = 0
fps_avg = [i for i in xrange(0,100)]
fps_avg_c = fps_avg_val = 0

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: 
            running = False
        if event.type == pygame.KEYUP:            
            if event.key == pygame.K_ESCAPE:
                running = False
        if event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:
                t = g.get_tile_at_pixelpos(event.pos)
                if t != None:
                    p = g.find_path (g.get_tile_at_tilepos(man[0].rect.tile_topleft), t)
                    
                    if p[0] == True:                        
                        path = p[1]
                        #print g.get_tile_at_tilepos(man[0].rect.tile_topleft), "p", path
                        tile = path.next() 
                        #print tile
                        for m in man:   
                            g.unregister (m, g.get_tile_at_tilepos(m.rect.tile_topleft))
                            if tile != None:
                                m.heading = g.get_heading_to_target(m.rect.topleft, tile)
                                m.rotate()
                            g.register (m, tile)
                    else:
                        tile = None
                    
    SCREEN.fill ((128,128,128))
    
    #g.draw(SCREEN)
    #for t in g.tiles:
    #    t.draw(SCREEN, g)
    for m in man:
        m.draw(SCREEN, g)
    
    if tile != None: 
        #print g.get_tile_at_pixelpos(man[0].rect.center), tile, man[0].heading
        m = man[0]
        #print tile, m.rect.tile_topleft

        if g.get_tile_at_tilepos (m.rect.tile_topleft) == tile:   
            if path != None: 
                tile = path.next()
                if tile != None: 
                    g.unregister(m, tile)
                    m.heading = g.get_heading_to_target(m.rect.topleft, tile)
                    m.rotate()
                    g.register(m, tile)
            
        else:
            for m in man:
                m.heading = g.get_heading_to_target(m.rect.topleft, tile)
                m.rotate()
                m.move_forward(1)
                #print "Man: ", m.rect.topleft, m.rect.tile_topleft
            
    for t in treelist:
        t.draw(SCREEN, g)
    
    timer.tick()
    pygame.display.flip()

pygame.quit()
sys.exit()