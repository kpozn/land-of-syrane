import lazyresource
import reshelper
import os
import random
import grid
import tree
import village

class World_creator (object):
    """Initialize world and set everything into a proper state"""
    def __init__(self, world):
        self.world = world
        self.grid = self.world.grid
        self.resHelper = reshelper.ResourceHelper(self.world)
        self.resMgr = lazyresource.ResourceManager()
        
    def create_world (self):
        #generate new village
        #self.village = village.Village (self.grid, 1, (20,20))
        #self.village.area_rectangle ((25,25))
        
        #self.village.set_village_center(self.resHelper)
#        
#        home = self.village.addHome(self.resHelper)
#        if home: self.village.addField(self.resHelper, home)
#
#        home = self.village.addHome(self.resHelper)
#        if home: self.village.addField(self.resHelper, home)
#
#        home = self.village.addHome(self.resHelper)
#        if home: self.village.addField(self.resHelper, home)
#
#        home = self.village.addHome(self.resHelper)
#        if home: self.village.addField(self.resHelper, home)

        self.world.background = self.resMgr.load_surface(os.path.join("res","gfx","background_desert3.png")).convert()
        
        self.resHelper.renderResourceShadow()
        self.resHelper.renderResourceDirt()
        
        self.create_woods()
        
        # add residents of village to self.man list to get AI control
        
        #self.world.sprite.extend(self.world.village.getResidents())
        
        ##self.random_place_tree()

    # functions to initlialize and gernerate random woods
    # -> see load / save functions for loading woods from files    
    def create_woods (self, amount=25):
        """
        create "amount" of tree.Wood objects and register them in the world.
        """
        for i in range (0, amount):
            x,y = random.randint (0, self.grid.WTi), random.randint (0, self.grid.HTi)
            self.world.wood.append(tree.Wood (self.world, i, (x,y)))
            for j in range (0,10):
                self.world.wood[-1].area_grow(25)
            self.world.wood[-1].generate_trees ()
            
  

    def createMultipleHumanoids (self, number, cType, tPosTi):
        tile = self.grid.getTileAtTi(tPosTi)
        objs = []
        for i in xrange (0, number):
            objs.append(cType(self.grid, tile.rPosPi.center, None, tile.getZDepth(), self.zBuffer))
        return objs

    def init_man (self):
        pass
#        dState = {"idle": "callbackIdle",
#                  "run": "callbackRun",
#                  "walk": "callbackWalk"
#                  }
#
#        tPosTi = (rndint(20)+10, rndint(20)+10)
#        man = self.createMultipleHumanoids (1, cWoodChopper, tPosTi)
#        c=0
#        for m in man:
#            #m.loadResources("johnny", dState)
#            m.setState("idle", "idle")
#            x = rndint (10)
#            y = rndint (20)
#            tile = self.grid.getTileAtTi((x,y))
#            self.grid.register (m,"mantype")
#            self.grid.register (m, "man"+str(c))
#            if tile != None:
#            #    m.setState("walk", tile) 
#            #    m.setSpeed(0.5)
#                self.grid.register(m, tile)
#            c+=1
#        return man


    def init_skeletons(self):
        dState = {"walk": "callbackWalk",
                  "attack": "callbackAttack"   }
        
        tPosTi = (rndint(20)+10, rndint(20)+10)
        skeleton = self.createMultipleHumanoids (2, cSkeleton, tPosTi)
        c = 0                     
        for m in skeleton:
            #m.loadResources("skeleton", dState)
            x = rndint (20)
            y = rndint (20)
            tile = self.grid.getTileAtTi((x,y))
            self.grid.register (m, "skeleton"+str(c))
            self.grid.register (m, "skeletontype")
            self.grid.register (m, "blocking")
            if tile != None:
                m.setState("walk", "walk", tile) 
                m.setSpeed(0.5)
            c+=1
        return skeleton