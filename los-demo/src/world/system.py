import pygame

class System (object):

    scr_res = scr_w, scr_h = (0,0)
    scr_depth = 0
    screen = None
    font =  {}
    
    def __init__ (self):
        pygame.init()
    
    def set_screen (self, width, height, flags=0):
        System.scr_res = System.scr_w, System.scr_h = (width, height)    
        System.scr_depth = pygame.display.mode_ok(System.scr_res)
        System.screen = pygame.display.set_mode(System.scr_res, flags, System.scr_depth)
    
    def register_font (self, name, size, filename):
        System.font[(name, size)] = pygame.font.Font (filename, size)        
    
    def get_font (self, name, size):
        return System.font [(name, font)] 
     
    def font_render (self, name, size, *font_parameters):
        return System.font [(name, font)].render (*font_parameters)
