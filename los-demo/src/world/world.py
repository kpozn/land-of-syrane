# DIRTY

import pygame

class World(object):
    """Datastore for all kinds of stuff"""
    def __init__(self):
        self.__grid = None
#        self.__zbuffer = None
        self.__sprite = []
        self.__village = []
        self.__background = None
        self.__wood = []

        # Memory for area groups we have // better be a dict?
        self.__area_group = []
        
        self.__render_layers = {}
        

    def getGrid(self):
        return self.__grid
#    def getZbuffer(self):
#        return self.__zbuffer
    def getSprite(self):
        return self.__sprite
    def getSprite_count(self):
        return len (self.__sprite)
    def setGrid(self, value):
        self.__grid = value
#    def setZbuffer(self, value):
#        self.__zbuffer = value
    def setSprite(self, value):
        self.__sprite = value
    def delGrid(self):
        del self.__grid
    def delZbuffer(self):
        del self.__zbuffer
    def delSprite(self):
        del self.__sprite
    def getVillage(self):
        return self.__village
    def setVillage(self, value):
        self.__village = value
    def delVillage(self):
        del self.__village
    def getBackground(self):
        return self.__background
    def setBackground(self, value):
        self.__background = value
    def getWood(self):
        return self.__wood
    def setWood(self, value):
        self.__wood = value
        
    def set_render_layer (self, name, surface, paramters):
        self.__render_layers[name] = (surface, paramters)
        
    def get_render_layer (self, name):
        return self.__render_layers[name]
        
    def has_render_layer (self, name):
        try:
            a = self.__render_layers[name]
            return True
        except:
            pass
        return False
    
    def del_render_layer (self, name):
        del self.__render_layers[name]
        
    grid = property(getGrid, setGrid, delGrid, "Grid's Docstring")
    #zbuffer = property(getZbuffer, setZbuffer, delZbuffer, "Zbuffer's Docstring")
    sprite = property(getSprite, setSprite, delSprite, "was: Man and sekeltions")
    sprite_count = property(getSprite_count, doc="Sprite_count's Docstring")
    village = property(getVillage, setVillage, delVillage, "Village's Docstring")
    background = property(getBackground, setBackground, doc="Village's Docstring")
    wood = property(getWood, setWood, doc="Wood's Docstring")

        # sprites
        #self.man = [] 
        #self.skeleton = []
        #self.man_count = 0
        #self.skel_count = 0
        #self.woods = []
    # put methods here that do some bookkeeping
    # properties for man and skelton
    # categories
    
    
# Some unit tests
if __name__ == "__main__":
    s = System()
    