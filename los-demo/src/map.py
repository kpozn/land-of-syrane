"""
Classes handling the map views and maps
"""

import grid
import pygame

class TileMap (object):
    def __init__ (self, world):
        self.world = world
        self.grid = self.world.grid
        self.map_alpha = 128
        
    def create_map_surface_from_grid (self):
        w,h = self.world.background.get_size()
        surface = pygame.Surface ((w,h))
        
        for y in range(0,self.grid.HTi):
            for x in range(0,self.grid.WTi):
                tile = self.grid.get_tile_at_tilepos((x,y))
                tags = self.grid.get_objs_for_tag (tile)
                color = (0,0,0)
                # parse tags and set colors
                for t in tags:
                    if isinstance (t, str) and t[:len("wood")]=="wood":
                        color = (0,255,0)
                    elif isinstance (t, str) and t[:len("village")]=="village":
                        color = (128,124,4)
                surface.fill (color, tile.rect)
        surface.set_alpha (self.map_alpha)
        return surface
        