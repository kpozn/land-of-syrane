# DIRTY

# MOVE TO GUI LIB
class ScreenInfo (object):
    """
    Info screen with infos to screen
    """
    def __init__ (self, grid, screen):
        self.grid = grid
        self.screen = screen
        self.imgVillageInfo = resMgr.loadSurface (os.path.join("res","gfx","gui","info.png")).convert()
        self.imgVillageInfo.set_colorkey ((255,0,255))
        self._showVillageInfo = False
    
    def showVillageInfo(self):
        self._showVillageInfo = True
    def hideVillageInfo(self):
        self._showVillageInfo = False
    def isVillageInfoShown(self):
        return self._showVillageInfo
    def displayVillageInfo (self, village):
        if self._showVillageInfo: 
            self.screen.blit (self.imgVillageInfo, (600, 10))
        else:
            self.screen.blit (self.imgVillageInfo, (770, 10))
